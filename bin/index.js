#! /usr/bin/env node

// Require
const shell = require("shelljs");
const inquirer = require("inquirer");
const chalk = require("chalk");
const figlet = require("figlet");

const init = () => {
    shell.exec("echo Welcome to MultiTool");

    console.log(
        chalk.green(
            figlet.textSync("Multi Tool", {
                font: "Big",
                horizontalLayout: "default",
                verticalLayout: "default"
            })
        )
    );
};

const askQuestions = () => {
    const questions = [
        {
            type: "list",
            name: "ADDTHEME",
            message: "Which files would you like to add?",
            choices: ["Theme", "Admin", "Content", "Data", "Config", "Assets", "All"]
        }
    ];
    return inquirer.prompt(questions);
};

const createTheme = () => {
    shell.rm('-rf', 'themes/multitool');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/multitool', 'themes/multitool');
};

const createAdmin = () => {
    shell.rm('-rf', 'static/admin');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/admin', 'static/admin');
};

const createContent = () => {
    shell.rm('-rf', 'content');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/content', 'content');
};

const createData = () => {
    shell.rm('-rf', 'data');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/data', 'data');
};

const createAssets = () => {
    shell.rm('-rf', 'assets');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/assets', 'assets');
};

const createConfig = () => {
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/config/package.json', 'package.json');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/config/config.toml', 'config.toml');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/config/.gitignore', '.gitignore');
    shell.cp('-R', './node_modules/@gotpop/multitool/dist/config/README.md', 'README.md');
};

const createAll = () => {
    createTheme();
    createAdmin();
    createContent();
    createData();
    createAssets();
    createConfig();
};

const success = () => {
    console.log(
        chalk.grey.bgGreen.bold(`Done! Base theme copied to themes folder.`)
    );
};

const run = async () => {
    init();

    const answers = await askQuestions();
    const {
        ADDTHEME
    } = answers;

    switch (ADDTHEME) {
        case 'Theme':
            createTheme();
            break;
        case 'Admin':
            createAdmin();
            break;
        case 'Content':
            createContent();
            break;
        case 'Data':
            createData();
            break;
        case 'Config':
            createConfig();
            break;
        case 'Assets':
            createConfig();
            break;
        case 'All':
            createAll();
            break;
    }

    success();
};

run();
